package cn;

import java.sql.*;

//https://cwiki.apache.org/confluence/display/Hive/HiveServer2+Clients#HiveServer2Clients-JDBC
public class HiveJdbcClient {
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";

    /**
     * @param args
     * @throws SQLException
     */
    public static void main(String[] args) throws SQLException {
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.exit(1);
        }
        //replace "hive" here with the name of the user the queries should run as
        Connection con = DriverManager.getConnection("jdbc:hive2://localhost:10000/default", "root", "root");

        Statement stmt = con.createStatement();
        ResultSet res = stmt.executeQuery("select * from hbase_table_2");
        boolean moreRow = res.next();
        while (moreRow) {
            System.out.println(res.getString(1)+","+res.getString(2));
            moreRow = res.next();
        }
    }
}